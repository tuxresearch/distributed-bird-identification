import os
import zipfile

for i in range(5):
    name = f'birdsong-resampled-train-audio-0{i}'
    os.system('kaggle d download ttahara/' + name)

    with zipfile.ZipFile(name+'.zip', 'r') as zip_ref:
        os.mkdir(name)
        zip_ref.extractall(name)

    os.unlink(name + '.zip')
